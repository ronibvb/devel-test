## Referências Técnicas

1. Quais foram os últimos dois livros técnicos que você leu?
Iam Sommerville - Engenharia de Software 6º edição
Use a Cabeça - Java

2. Quais foram os últimos dois framework/CMS que você trabalhou?
Send
SilverStripe

3. Descreva os principais pontos positivos do seu framework favorito.
Gosto muito do Zend pela facilidade de utilizar o sistema na arquitetura MVC

4. Descreva os principais pontos negativos do seu framework favorito.
Acho ele meio lento e pouca documentação para estudo

5. O que é código de qualidade para você.
Código de qualidade deve ser de qualidade ou seja rápido, bem legível, utilizando sempre as boas práticas de engenharia de software 
e que faça funcione corretamente conforme o esperado pelo usuário. 

## Conhecimento Linux

1. O que é software livre?
Um software livre é quando qualquer pessoa pode modificar, copiar e redistribuir o mesmo.

2. Qual o seu sistema operacional favorito?
Slackware 

3. Já trabalhou com Linux ou outro Unix-like?
SIM, Slackware, Ubuntu e Debian

4. O que é SSH?
É um protocolo que realiza a comunicação com 2 hosts diferentes através de uma autenticação segura.

5. Quais as principais diferenças entre sistemas *nix e o Windows?
No Unix você tem acesso ao código e no windows o acesso é restrito.
O windows é pago e unix é free.


## Conhecimento de desenvolvimento

1. O que é GIT?
É um  sistema de controle de versão, seu maior objetivo é facilitar e integrar a vida dos programadores garantindo uma maior confiabilidade.

2. Descreva um workflow simples de trabalho utilizando GIT.
Centralized Workflow 

3. O que é PHP Data Objects?
É um módulo PHP no paradigma OO seu maior objetivo é manter a padronização de forma que o PHP consiga se comunicar com o BD.
 
4. O que é Database Abstract Layer?
Não sei

5. Você sabe o que é Object Relational Mapping? Se sim, explique.
Sim, é uma técnica muito utilizada de objeto relacional permite fazer as relações dos objetos de forma mais organizada e eficientes.

6. Como você avalia seu grau de conhecimento em Orientação a objeto?
Avançado, tenho uma ampla experiência na área.

7. O que é Dependency Injection?
A injeção de dependência é um design pattern que visa desacoplar os componentes da aplicação

8. O significa a sigla S.O.L.I.D?
Não sei

9. Qual a finalidade do framework PHPUnit?
Realizar testes unitários de classes PHP

10. Explique o que é MVC.
MVC é um padrão de arquitetura para o desenvolvimento de sistemas orientado a objetos, tendo as camadas Model(abstração BD), View(interface com usuário), Controller(responsável de fazer a comunicação entre as camadas.) 